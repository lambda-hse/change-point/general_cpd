# General CPD

## Description
### `/data`
This directory contains samples for time series with change points.

### `/paper01`
This directory contains all scripts, tests and plots for algorithms described in paper \
**Generalization of Change-Point Detection in Time Series Data Based on Direct Density Ratio Estimation** https://arxiv.org/abs/2001.06386


## Public datasets
- NAB: https://github.com/numenta/NAB
- UCI: https://archive.ics.uci.edu/ml/datasets.php
